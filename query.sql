-- Jumlah Complain per bulan
select
	YEAR(`C1`) as tahun,
	MONTH(`C1`) as bulan,
	COUNT(`C1`) as jumlah
from `ConsumerComplaints` group by YEAR(`C1`), MONTH(`C1`)
order by YEAR(`C1`), MONTH(`C1`);

-- Complain dengan tag older americas
SELECT * FROM `ConsumerComplaints`
WHERE C11 LIKE '%Older American%';

-- Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah
